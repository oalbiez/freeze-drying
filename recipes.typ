#set page(
  paper: "a4",
    margin: (
      top: 1cm,
      bottom: 1cm,
      left: 1cm,
      right: 1cm,
    ),
)

#let stick-together(a, b, threshold: 3em) = {
  block(a + v(threshold), breakable: false)
  v(-1 * threshold)
  b
}

#let recipes(
  title: "",
  steps: ()
  ) = {

  let colors = (silver,)
  let cells = ()
  for (i, s) in steps.enumerate() {
    cells.push(str(i+1))
    cells.push(str(s.at(0)))
    cells.push(str(s.at(1)))
    if s.at(2) {
      cells.push("Yes")
      colors.push(aqua)
    } else {
      cells.push("No")
      colors.push(white)
    }
  }

  stick-together(
    title,
    table(
      columns: (auto, auto, auto, auto),
      stroke: gray,
      inset: 2mm,
      align: horizon+center,
      fill: (col, row) => colors.at(row),
      [*Step*], [*°C*], [*Minutes*], [*Vaccum*],
      ..cells
    )
  )
}


#show: columns.with(2)


== Fruits et légumes

#recipes(
  title: [=== Générique],
  steps: (
    ( -35 , 360 , false),
    ( -20 , 60 , true),
    ( 0 , 120 , true),
    ( 25 , 180 , true),
    ( 45 , 480 , true),
    ( 55 , 900 , true),
))

#recipes(
  title: [=== Générique alternative],
  steps: (
    ( -45 , 300 , false),
    ( -30 , 60 , true),
    ( -10 , 60 , true),
    ( 20 , 300 , true),
    ( 50 , 900 , true),
))

#recipes(
  title: [=== Bananes / Fruits du dragon / Aubépine],
  steps: (
    ( -35 , 420 , false),
    ( -20 , 60 , true),
    ( 0 , 120 , true),
    ( 25 , 180 , true),
    ( 45 , 480 , true),
    ( 55 , 900 , true),
))

#recipes(
  title: [=== Mangue],
  steps: (
    ( -45 , 480 , false),
    ( -30 , 60 , true),
    ( -20 , 60 , true),
    ( -10 , 60 , true),
    ( 0 , 60 , true),
    ( 10 , 180 , true),
    ( 20 , 180 , true),
    ( 30 , 180 , true),
    ( 40 , 180 , true),
    ( 45 , 180 , true),
    ( 50 , 300 , true),
    ( 55 , 300 , true),
))


#colbreak()

== Viandes

#recipes(
  title: [=== Générique],
  steps: (
    ( -35 , 420 , false),
    ( -5 , 60 , true),
    ( 25 , 120 , true),
    ( 40 , 480 , true),
    ( 50 , 1080 , true),
))


== Liquides

#recipes(
  title: [=== Générique],
  steps: (
    ( -35 , 480 , false),
    ( -25 , 60 , true),
    ( -15 , 60 , true),
    ( -5 , 60 , true),
    ( 0 , 180 , true),
    ( 5 , 120 , true),
    ( 10 , 120 , true),
    ( 15 , 120 , true),
    ( 20 , 120 , true),
    ( 25 , 180 , true),
    ( 30 , 300 , true),
    ( 35 , 300 , true),
))

#recipes(
  title: [=== Lait et yogourt],
  steps: (
    ( -35 , 480  , false),
    ( -24 , 60   , true),
    ( -15 , 60   , true),
    ( -05 , 60   , true),
    ( +00 , 180  , true),
    ( +05 , 120  , true),
))
